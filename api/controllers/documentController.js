'use documents';

var mongoose = require('mongoose'),
Document = mongoose.model('Document');

exports.findAll = function(req, res) {
  Document.find({}, function(err, task) {
    console.log("CALL OF route api");
    if (err)
      res.send(err);
    res.json(task);
  });
};
