'use documents';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var doucumentSchema = new Schema({
  meter: {
    link:   String,
    img:    String,
    type:   String,
    detail: String
  },
  body: {
    authorLink: String,
    authorImg:  String,
    authorName: String,
    detail:     String,
    detailLink: String,
    title:      String,
    link:       String,
    editor:     String,
    editorLink: String,
    date:       String
  }
});

module.exports = mongoose.model('Document', doucumentSchema);
//{ meter: { link: "", img: "", type: "", detail: "" }, body: { authorLink: "", authorImg: "", authorName: "", detail: "", detailLink: "", title: "", link: "", editor: "", editorLink: "", date: "" }}
