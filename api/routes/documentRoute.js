'use docu';
module.exports = function(app) {
  var controller = require('../controllers/documentController');

  // Routes
  app.route('/api')
    .get(controller.findAll);
};
