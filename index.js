require('dotenv').config();

var express   = require('express'),
  fs          = require('fs'),
  request     = require('request')
  cheerio     = require('cheerio'),
  mongoose = require('mongoose'),
  bodyParser  = require('body-parser'),
  //api         = require('./routes/api'),
  app         = express();
  Document    = require('./api/models/documentModel'); //created model loading here

var docs = [];


// mongoose instance connection url connection
//console.log("LINK ====================> "+uri);
//mongoose.Promise = global.Promise;
mongoose.connect(process.env.DB_URI, {
    useMongoClient: true
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./api/routes/documentRoute'); //importing route
routes(app); //register the route


app.listen("8081");
console.log('Docu list RESTful API server started on: 8081');
