require('dotenv').config();

var express   = require('express'),
  fs          = require('fs'),
  request     = require('request')
  cheerio     = require('cheerio'),
  mongo       = require('mongodb'),
  mongoClient = require('mongodb').MongoClient,
  bodyParser  = require('body-parser'),
  //api         = require('./routes/api'),
  app         = express();
  Document    = require('./api/models/documentModel'); //created model loading here

var docs = [];

app.get('/scrape', function(req, res){

    url = 'http://www.politifact.com/truth-o-meter/statements/?page=';

    var i;
    for(i = 1; i < 695; i++){
        link = url+i;
        pageExtractor(link);
    }

    if(docs.length != 0){
      console.log("DOCS ARRAY SIZE ============> "+docs.length);
      insert(docs);
      /**
        fs.writeFile('output.json', JSON.stringify(docs, null, 4), function(err){
            console.log('File successfully written! - Check your project directory for the output.json file');
        }) */
    }
})

function insert(data){
  mongoClient.connect(process.env.DB_URI, function(error, db) {
    if (error) throw error;
    console.log("Connecté à la base de données '21308390_bd'");
    /**
    db.createCollection("documents", function(err, res) {
      if (err) throw err;
      console.log("Collection created!");
      db.close();
    });*/

    db.collection("docu").insertMany(data, function(err, res) {
      if (err) throw err;
      console.log("Number of documents inserted: " + res.insertedCount);
      db.close();
    });
  });
}


/**
 * Extract data on page
 *
 * @param {*String} url
 */
function pageExtractor(url){
    request(url, function(error, response, html){
        if(!error){

            var $ = cheerio.load(html);
            $('.scoretable__item').filter(function(){
                var dom = $(this);
                docs.push(extractItemData(dom));
            })
        }
    })
}


/**
 * Extract document item in DOM
 *
 * @param {*} data
 */
function extractItemData(data){
    var json = { meter: { link: "", img: "", type: "", detail: "" }, body: { authorLink: "", authorImg: "", authorName: "", detail: "", detailLink: "", title: "", link: "", editor: "", editorLink: "", date: "" }};

    var meter = data.children().first().children().first();
    var statement_body = meter.next().children();
    var statement_body_first_child = statement_body.first();

    json.meter.link = meter.children('a').attr('href');
    json.meter.img = meter.children('a').children('img').attr('src');
    json.meter.type = meter.children('a').children('img').attr('alt');
    json.meter.detail = meter.children('p').text();

    json.body.authorLink = statement_body_first_child.children('a').attr('href');
    json.body.authorImg = statement_body_first_child.children('a').children('img').attr('src');
    json.body.authorName = statement_body_first_child.children('a').children('img').attr('alt');

    json.body.detailLink = statement_body_first_child.next().children('a').attr('href');
    json.body.detail = statement_body_first_child.next().children('a').text();

    json.body.title = statement_body_first_child.next().next().children('a').text();
    json.body.link = statement_body_first_child.next().next().children('a').attr('href');

    json.body.editor = statement_body_first_child.next().next().next().children('a').text();
    json.body.editorLink = statement_body_first_child.next().next().next().children('a').attr('href');
    json.body.date = statement_body_first_child.next().next().next().children('span').text();

    return json;
}

app.listen('8081')
console.log('Magic happens on port 8081');
exports = module.exports = app;
